# Compile i3 From Source
1. git clone https://www.github.com/i3/i3 [ original version] [ https://github.com/Airblader/i3 [ fork version with i3-gaps ]
2. cd i3
3. mkdir -p build && cd build
4. meson ..
5. ninja
---
# Debian dependencies
1. sudo apt install meson dh-autoreconf libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev libxcb-icccm4-dev libyajl-dev libev-dev libxcb-xkb-dev libxcb-cursor-dev libxkbcommon-dev libxcb-xinerama0-dev libxkbcommon-x11-dev libstartup-notification0-dev libxcb-randr0-dev libxcb-xrm0 libxcb-xrm-dev libxcb-shape0 libxcb-shape0-dev
2. cpan install Pod::Simple AnyEvent::I3 JSON::XS
---
# Create symlink for your i3.
**In build directory creates symlinks that point to /usr/bin/ directory.**

1. sudo ln -s $(pwd)/i3  /usr/bin/i3
2. sudo ln -s $(pwd)/i3bar /usr/bin/i3bar
3. sudo ln -s $(pwd)/i3-config-wizard /usr/bin/i3-config-wizard
4. sudo ln -s $(pwd)/i3-dump-log /usr/bin/i3-dump-log
5. sudo ln -s $(pwd)/i3-input /usr/bin/i3-input
6. sudo ln -s $(pwd)/i3-msg /usr/bin/i3-msg
7. sudo ln -s $(pwd)/i3-nagbar /usr/bin/i3-nagbar

**Now one step back from build directory [ cd .. ]. Now we are in i3 base directory and creates symlinks that point to /usr/bin/ directroy**

8. sudo ln -s $(pwd)/i3-dmenu-desktop /usr/bin/i3-dmenu-desktop
9. sudo ln -s $(pwd)/i3-save-tree /usr/bin/i3-save-tree
10. sudo ln -s $(pwd)/i3-sensible-editor /usr/bin/i3-sensible-editor
11. sudo ln -s $(pwd)/i3-sensible-pager /usr/bin/i3-sensible-pager
12. sudo ln -s $(pwd)/i3-sensible-terminal /usr/bin/i3-sensible-terminal
---
# i3blocks compile from source
1. git clone https://github.com/vivien/i3blocks
2. cd i3blocks
3. ./autogen.sh
4. ./configure
5. make
6. sudo make install
# Create symlink for i3blocks.
1. sudo ln -s $(pwd)/i3blocks /usr/local/bin/ 

---
# Configure i3

1. mkdir -p ~/.config/i3/
2. cp ~/i3/etc/config ~/.config/i3/config [ in this directory ] 

**Download my i3wm config file and copy to the ~/.config/i3/config**

## Configure i3blocks

1. mkdir -p ~/.config/i3blocks
2. cp ~/i3blocks/i3blocks.conf ~/.config/i3blocks/i3blocks.conf

**or**

3. you can edit with root privilege /usr/local/etc/i3blocks.conf file

**Download my i3blocks config file and copy to ~/.config/i3blocks/i3blocks.conf or /usr/local/etc/i3blocks.conf**

# Each time there is an update, you have to do the following steps in your i3 and i3blocks directory:
**i3**

**Which branch to use?
Work on i3 generally happens in two branches: “next” (default) and “stable”.
The contents of “stable” are always stable. That is, it contains the source code of the latest release, plus any bugfixes that were applied since that release.
New features are only found in the “next” branch. Always use this branch when writing new code (both bugfixes and features).**

1. git pull origin next or stable [ note for i3-gaps branch is gaps-next ]

**After update git branch you need to compile or build again**

2. cd i3/build
3. meson ..
4. ninja

**i3blocks**

1. git pull origin master
2. cd i3blocks
3. ./autogen.sh
4. ./configure
5. make
6. sudo make install
---
# Some Usefull tools

1. nitrogen [ For wallpaper ]
2. dunst [ For Notifications ]
3. numlockx [ For Numlock ]
4. compton/picom [ For Transparency/shadow/rounded_corner ]
5. dmenu/rofi [ For Menu ]